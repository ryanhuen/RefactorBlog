from .common import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'development-secret-key'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

RELEASE_DOMAIN = '66.112.212.14'

ALLOWED_HOSTS = ['*']
