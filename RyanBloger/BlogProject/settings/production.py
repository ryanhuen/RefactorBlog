from .common import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '$l8m#8e%$4o=b+sc#ufjh^uod!xux441w*f@crp_e1ztl+9_-u'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

RELEASE_DOMAIN = '66.112.212.14'

ALLOWED_HOSTS = ['127.0.0.1', 'localhost ', RELEASE_DOMAIN, '.ryanhuen.tech']
