"""RyanHuenBlog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib.sitemaps import GenericSitemap
from django.contrib.sitemaps.views import sitemap
from django.views.generic.base import TemplateView

import xadmin
from BlogProject.views import HomeView
from articles.models import Articles

info_dict = {
    'queryset': Articles.objects.all(),
    'date_field': 'modify_time',
}

urlpatterns = [
    url(r'^xadmin/', xadmin.site.urls),
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^tags/$', TemplateView.as_view(template_name='tags.html'), name='tags'),
    #
    url(r'^blog/', include('articles.urls', namespace='blog')),
    url(r'^haystack_search/', include('haystack.urls')),
    #
    # # 配置上传文件的访问处理函数
    # url('^media/(?P<path>.*)/$', serve, {'document_root': MEDIA_ROOT})
    # the sitemap
    url(r'sitemap.xml', sitemap,
        {'sitemaps': {'blog': GenericSitemap(info_dict, priority=0.6)}},
        name='django.contrib.sitemaps.views.sitemap'),
    url(r'mdeditor/', include('mdeditor.urls')),
]
