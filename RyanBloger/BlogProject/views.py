from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import View

from BlogStatistics.decorator.StatisticDecorator import intercept_site_visitor
from BlogStatistics.models import DuplicateVisit, SiteVisitor


@method_decorator(intercept_site_visitor, name='get')
class HomeView(View):
    def get(self, request):
        try:
            site_visit = DuplicateVisit.objects.get(type='site').count
        except Exception:
            site_visit = 0
        try:
            visitor = SiteVisitor.objects.count()
        except Exception:
            visitor = 0
        return render(request, 'home.html', {
            'site_visit': site_visit,
            'visitor': visitor,
        })
