import re


def go():
    rex = r'\[//\]\:\s+(?P<key>.*)\s+(:)\s+(?P<value>[\w\w].*)'
    pattern = re.compile(rex)
    test_key = r'[//]: title : 举个Java回调的栗子'
    if pattern.match(test_key):
        a = pattern.match(test_key).group('key')
        print(a)
    else:
        print('failed')


if __name__ == '__main__':
    go()
