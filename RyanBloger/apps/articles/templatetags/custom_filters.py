__author__ = 'ryanhuen'
__data__ = '18-3-26 下午3:40'

from django import template

register = template.Library()


@register.filter
def filter_tags(v1, v2):
    return v2.filter(tags=v1).count()


@register.filter
def filter_category(v1, v2):
    return v2.filter(category_id=v1).count()
