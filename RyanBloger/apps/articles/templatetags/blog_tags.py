# 获取最新5篇文章
from django import template
from django.db.models.aggregates import Count

from BlogProject.settings.common import SITE_CONFIG
from articles.models import Articles, Tag, Links, Category

register = template.Library()


@register.simple_tag(takes_context=True)
def get_recent_posts(context, num=5):
    request = context.request
    if request and request.user.is_authenticated:
        return Articles.objects.all().order_by('-modify_time')[:num]
    else:
        return Articles.objects.filter(export=True).order_by('-modify_time')[:num]


@register.simple_tag
def site_config():
    return SITE_CONFIG


@register.simple_tag
def get_tags():
    # 使用 Count 方法统计文章数，并保存到 num_posts 属性中
    tags_style = [
        "font-size:19px;color:#777",
        "font-size:14px;color:#999",
        "font-size:16.5px;color:#888",
        "font-size:24px;color:#555",
        "font-size:21.5px;color:#666"
    ]
    tags = Tag.objects.all().annotate(num_articles=Count('articles')).filter(num_articles__gt=0).order_by(
        '-num_articles')[:20]
    return {'tags_style': tags_style, 'tags': tags}


@register.simple_tag
def get_categorys():
    # 使用 Count 方法统计文章数，并保存到 num_posts 属性中
    category_style = [
        "font-size:19px;color:#777",
        "font-size:14px;color:#999",
        "font-size:16.5px;color:#888",
        "font-size:24px;color:#555",
        "font-size:21.5px;color:#666"
    ]
    categorys = Category.objects.all()[:20]
    return {'category_style': category_style, 'categorys': categorys}


# 获取友情链接
@register.simple_tag
def get_links():
    return Links.objects.filter(is_show=True)


@register.simple_tag
def filter_count(article):
    return {'visitor': article.visitor.values('ip').distinct().count(), 'total_read': article.visitor.count()}
