from datetime import datetime

import markdown
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User
from django.db import models
from django.utils.html import strip_tags
from mdeditor.fields import MDTextField

from BlogStatistics.models import ArticleVisitor
from utils import CommonUtils


# Create your models here.

# Create your models here.
class UserProfile(AbstractUser):
    nick_name = models.CharField(max_length=50, verbose_name=u'昵称', default='')

    class Meta:
        verbose_name = u'用户信息'
        verbose_name_plural = verbose_name

    def __str__(self):
        # python3中，由于所有字段都被认为是unicode，所以不需要像python2一样适配
        return self.username


class Category(models.Model):
    name = models.CharField(max_length=100, verbose_name='分类名称')

    class Meta:
        verbose_name = '文章分类'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=100, verbose_name='标签名称')

    class Meta:
        verbose_name = '文章标签'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class Articles(models.Model):
    title = models.CharField(max_length=50, verbose_name='文章标题')
    content = MDTextField()
    public_time = models.DateTimeField(null=False, default=datetime.now, verbose_name='发布时间')
    modify_time = models.DateTimeField(null=False, default=datetime.now, verbose_name='修改时间')
    summary = models.TextField(null=True, blank=True, verbose_name='摘要')
    category = models.ForeignKey(Category, verbose_name='文章分类', on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag, verbose_name='文章标签')
    export = models.BooleanField(blank=False, null=False, default=False, verbose_name='非管理员是否可读')
    author = models.ForeignKey(UserProfile, on_delete=models.CASCADE, verbose_name='作者')
    article_type = models.CharField(choices=(('project', u'项目'), ('normal', u'普通文章')), default='project', max_length=7,
                                    verbose_name='文章类型')
    visitor = models.ManyToManyField(ArticleVisitor, editable=False, verbose_name='访问者')

    class Meta:
        verbose_name = '博客文章'
        verbose_name_plural = verbose_name
        ordering = ['-public_time']

    def save(self, *args, **kwargs):
        self.summary = CommonUtils.org2html(self.get_right_summary_src())
        super(Articles, self).save(*args, **kwargs)

    def get_right_summary_src(self):
        # 首先实例化一个 Markdown 类，用于渲染 body 的文本。
        # 由于摘要并不需要生成文章目录，所以去掉了目录拓展。
        md = markdown.Markdown(extensions=[
            'markdown.extensions.extra',
            'markdown.extensions.codehilite',
        ])
        # 先将 Markdown 文本渲染成 HTML 文本
        # strip_tags 去掉 HTML 文本的全部 HTML 标签
        # 从文本摘取前 54 个字符赋给 excerpt
        self.summary = strip_tags(md.convert(self.content))[:54]
        return self.summary
        # end_config_index = self.content.find('END_CONFIG') + 9
        # # 去除配置项后，如果content内容小于200，预览就可以完整展现文章
        # if len(self.content) - end_config_index < 200:
        #     size = len(self.content)
        #     return self.content[:size]
        # begin_index = self.content.find('#+BEGIN')
        # if 200 >= begin_index > 0:
        #     return self.content[:begin_index]
        # else:
        #     return self.content[:200]

    def get_absolute_url(self):
        return r'/blog/article/%d' % self.id

    def __str__(self):
        return self.title


# class UpdateDatabaseConfig(models.Model):
#     public_time = models.DateTimeField(null=False, verbose_name='发布时间', auto_now_add=True)
#     modified_time = models.DateTimeField(null=False, verbose_name='最后修改', auto_now=True)
#
#     def update_all_article(self):
#         file = FileList()
#         file.print_files(os.path.join(BASE_DIR, 'RyanNote'))
#         for path in file.file_list:
#             content = CommonUtils.read_file_content(path)
#             org_config = UpdateArticleUtils.read_org_file_config_content(path)
#             if org_config:
#                 if not org_config.inner_title:
#                     continue
#                 else:
#                     try:
#                         article = Articles.objects.filter(inner_title=org_config.inner_title)[0]
#                         if article.md5_hash == UpdateArticleUtils.check_file_content_md5(content):
#                             continue
#                     except IndexError as e:
#                         article = Articles()
#                     if not article:
#                         article = Articles()
#                     article.inner_title = org_config.inner_title
#                     article.title = org_config.title
#                     article.content = content
#                     if not Category.objects.filter(name=org_config.category):
#                         category = Category()
#                         category.name = org_config.category
#                         category.save()
#                     else:
#                         category = Category.objects.filter(name=org_config.category)[0]
#
#                     article.category = category
#                     article.export = org_config.export
#                     article.save()
#                     for tag_name in org_config.tags:
#                         if Tag.objects.filter(name=tag_name):
#                             tag = Tag.objects.filter(name=tag_name)[0]
#                         else:
#                             tag = Tag()
#                             tag.name = tag_name
#                             tag.save()
#                         article.tags.add(tag)
#                     article.save()
#         file.file_list.clear()
#
#     def save(self, *args, **kwargs):
#         self.update_all_article()
#         super(UpdateDatabaseConfig, self).save(*args, **kwargs)


class About(models.Model):
    title = models.CharField(max_length=50, verbose_name='文章标题')
    content = MDTextField()
    public_time = models.DateTimeField(null=False, verbose_name='发布时间', auto_now_add=True)
    modified_time = models.DateTimeField(null=False, verbose_name='最后修改', auto_now=True)
    summary = models.CharField(max_length=200, null=True, blank=True, verbose_name='摘要')

    class Meta:
        verbose_name = '关于本人'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.title


# 友情链接
class Links(models.Model):
    name = models.CharField(max_length=100, verbose_name=u'站点名称')
    url = models.URLField(max_length=225, verbose_name=u'站点链接')
    is_show = models.BooleanField(default=True, verbose_name=u'是否展示')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = u'友情链接'
        verbose_name_plural = u'友情链接'
