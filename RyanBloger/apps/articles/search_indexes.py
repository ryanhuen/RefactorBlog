from haystack import indexes

from articles.models import Articles

__author__ = 'ryanhuen'
__data__ = '18-4-28 下午5:49'


class ArticlesIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)

    def get_model(self):
        return Articles

    def index_queryset(self, using=None):
        return self.get_model().objects.all()

