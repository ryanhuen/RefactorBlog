import markdown
from django.db.models import Count, Q
from django.http import HttpResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.generic import View
from pure_pagination import Paginator, PageNotAnInteger
import json, re
from haystack.views import SearchView

from BlogStatistics.decorator.StatisticDecorator import intercept_site_visitor, intercept_article_visitor
from articles.models import Articles, Category, Tag, About
# Create your views here.
from utils import RequestUtils


@method_decorator(intercept_site_visitor, name='get')
class IndexView(View):
    def get(self, request):
        current_page = 'archive'
        # 获取全部文章
        if request.user.is_authenticated:
            all_articles = Articles.objects.all().order_by('-public_time')
        else:
            all_articles = Articles.objects.filter(export=True).order_by('-public_time')
        # 历程文章分页
        try:
            page = request.GET.get('page', 1)
        except PageNotAnInteger:
            page = 1

        p = Paginator(all_articles, per_page=10, request=request)

        paged_articles = p.page(page)
        tags = Tag.objects.all()
        categories = Category.objects.all()
        return render(request, 'blog/index.html', {
            'all_articles': paged_articles,
            'current_page': current_page,
            'total_pages': p.num_pages,
            'tags': tags,
            'categories': categories,
        })


@method_decorator(intercept_site_visitor, name='get')
class ArchiveView(View):
    def get(self, request):
        if request.user.is_authenticated:
            year_list = Articles.objects.filter(article_type='normal').dates('public_time', 'year', order='DESC')
            article_list = Articles.objects.filter(article_type='normal').order_by('-public_time')
        else:
            year_list = Articles.objects.filter(export=True, article_type='normal').dates('public_time', 'year',
                                                                                          order='DESC')
            article_list = Articles.objects.filter(export=True, article_type='normal').order_by('-public_time')

        try:
            page = request.GET.get('page', 1)
        except PageNotAnInteger:
            page = 1

        p = Paginator(article_list, per_page=10, request=request)

        paged_articles = p.page(page)
        article_list = paged_articles.object_list
        result_year = []
        for article in article_list:
            for year in year_list:
                if year.year == article.public_time.year and not result_year.__contains__(year):
                    result_year.append(year)
        return render(request, 'blog/archives.html', {
            'year_list': result_year,
            'article_list': paged_articles,
            'total_pages': p.num_pages,
        })


@method_decorator(intercept_article_visitor, name='request_article')
@method_decorator(intercept_site_visitor, name='get')
class ArticleDetailView(View):
    def get(self, request, article_id):
        penetrate_data = {
            'visitor': 0,
            'total_read': 0
        }
        try:
            article = Articles.objects.get(id=int(article_id))
            penetrate_data['visitor'] = article.visitor.values('ip').distinct().count()
            penetrate_data['total_read'] = article.visitor.count()
        except Exception:
            return RequestUtils.page_not_found(request)
        if request.user.is_authenticated:
            return self.request_article(True, penetrate_data, article, request)
        else:
            if not article.export:
                return RequestUtils.page_not_found(request)
            else:
                return self.request_article(False, penetrate_data, article, request)

    def request_article(self, auth, penetrate_data, article, request):
        md = markdown.Markdown(extensions=[
            'markdown.extensions.extra',
            'markdown.extensions.codehilite',
            'markdown.extensions.toc',
        ])
        content = md.convert(article.content)
        article_type = article.article_type
        kwargs = {'public_time__gt': article.public_time, 'article_type': article_type}
        if not auth:
            kwargs['export'] = True
        pri_article = Articles.objects.filter(**kwargs).order_by('public_time')[:1]
        if pri_article:
            pri_article = pri_article[0]
        kwargs = {'public_time__lt': article.public_time, 'article_type': article_type}
        if not auth:
            kwargs['export'] = True
        next_article = Articles.objects.filter(**kwargs).order_by('-public_time')[:1]
        if next_article:
            next_article = next_article[0]
        category = article.category
        return render(request, 'blog/detail.html', {
            'article': article,
            'content': content,
            'pri_article': pri_article,
            'next_article': next_article,
            'category': category,
            'visitor': penetrate_data['visitor'],
            'total_read': penetrate_data['total_read'],
            'export': article.export,
        })


@method_decorator(intercept_site_visitor, name='get')
class AllArticlesByCategoryView(View):
    def get(self, request, category_name):
        if request.user.is_authenticated:
            year_list = Articles.objects.filter(category__name=category_name).dates('public_time', 'year', order='DESC')
            article_list = Articles.objects.filter(category__name=category_name)
        else:
            year_list = Articles.objects.filter(export=True, category__name=category_name).dates('public_time', 'year',
                                                                                                 order='DESC')
            article_list = Articles.objects.filter(export=True, category__name=category_name)

        tags = Tag.objects.all()
        categories = Category.objects.all()
        return render(request, 'blog/tag.html', {
            'article_list': article_list,
            'year_list': year_list,
            'tags': tags,
            'categories': categories,
        })


@method_decorator(intercept_site_visitor, name='get')
class AllArticleByTagView(View):
    def get(self, request, tag_name):
        if request.user.is_authenticated:
            year_list = Articles.objects.filter(tags__name=tag_name).dates('public_time', 'year', order='DESC')
            article_list = Articles.objects.filter(tags__name=tag_name)
        else:
            year_list = Articles.objects.filter(export=True, tags__name=tag_name).dates('public_time', 'year',
                                                                                        order='DESC')
            article_list = Articles.objects.filter(export=True, tags__name=tag_name)

        tags = Tag.objects.all()
        categories = Category.objects.all()
        return render(request, 'blog/tag.html', {
            'article_list': article_list,
            'year_list': year_list,
            'tags': tags,
            'categories': categories,
        })


@method_decorator(intercept_site_visitor, name='get')
class AboutMeView(View):
    def get(self, request):
        about = About.objects.all()[0]
        md = markdown.Markdown(extensions=[
            'markdown.extensions.extra',
            'markdown.extensions.codehilite',
            'markdown.extensions.toc',
        ])
        content = md.convert(about.content)
        return render(request, 'blog/about.html', {
            'about': about,
            'content': content,
        })


@method_decorator(intercept_site_visitor, name='get')
class ProjectView(View):
    def get(self, request):
        if request.user.is_authenticated:
            year_list = Articles.objects.filter(article_type='project').dates('public_time', 'year', order='DESC')
            article_list = Articles.objects.filter(article_type='project')
        else:
            year_list = Articles.objects.filter(export=True, article_type='project').dates('public_time', 'year',
                                                                                           order='DESC')
            article_list = Articles.objects.filter(export=True, article_type='project')
        return render(request, 'blog/project.html', {
            'year_list': year_list,
            'article_list': article_list
        })
        # if request.user.is_authenticated:
        #     all_project_item = Articles.objects.filter(category__name='我做过的项目')
        # else:
        #     all_project_item = Articles.objects.filter(export=True).filter(category__name='我做过的项目')
        # return render(request, 'blog/project.html', {
        #     'all_project_item': all_project_item,
        # })


@method_decorator(intercept_site_visitor, name='get')
class AllTagsView(View):
    def get(self, request):
        all_tags = Tag.objects.all()
        return render(request, 'blog/tags.html', {
            'all_tags': all_tags,
        })


@method_decorator(intercept_site_visitor, name='get')
class AllCategorysView(View):
    def get(self, request):
        all_category = Category.objects.all()
        return render(request, 'blog/categorys.html', {
            'all_category': all_category,
        })


# 搜索请求
class MySearch(SearchView):
    def get(self, request):
        return super(self, request)


def search(request, ):
    # 搜索内容
    if request.method == 'GET':

        q = request.GET.get('q')
        post_list = Articles.objects.filter(Q(title__icontains=q) | Q(content__icontains=q))

        data = {'posts': []}
        for post in post_list:
            data['posts'].append({
                "title": post.title,
                "permalink": post.get_absolute_url(),
                "text": post.content
            }, )
        return HttpResponse(json.dumps(data, ensure_ascii=False), content_type="application/json,charset=utf-8")
    else:
        pass
