from django.conf.urls import url

from articles.views import ArticleDetailView, ArchiveView, AllArticlesByCategoryView, AllArticleByTagView, \
    AboutMeView, IndexView, ProjectView, AllTagsView, AllCategorysView, search

__author__ = 'ryanhuen'
__data__ = '18-3-26 下午4:20'
urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^article/(?P<article_id>\d+)/$', ArticleDetailView.as_view(), name='article'),
    url(r'^archives/$', ArchiveView.as_view(), name='archives'),
    url(r'^categorys/', AllCategorysView.as_view(), name='categorys'),
    url(r'^category/(?P<category_name>\w+)$', AllArticlesByCategoryView.as_view(), name='category'),
    url(r'^tags/', AllTagsView.as_view(), name='tags'),
    url(r'^tag/(?P<tag_name>\w+)$', AllArticleByTagView.as_view(), name='tag'),
    url(r'^about/$', AboutMeView.as_view(), name='about'),
    url(r'^project/$', ProjectView.as_view(), name='project'),
    url('content.json/', search, name='search'),

]
app_name = 'articles'
