import xadmin
from articles.models import Articles, Category, Tag, About

__author__ = 'ryanhuen'
__data__ = '18-3-19 下午3:49'


class ArticlesAdmin(object):
    list_display = ['title', 'public_time', 'summary', 'category', 'tags']
    search_fields = ['title']
    list_filter = ['title', 'public_time', "category", 'tags']


class CategoryAdmin(object):
    list_display = ['name']
    search_fields = ['name']
    list_filter = ['name']


class TagAdmin(object):
    list_display = ['name']
    search_fields = ['name']
    list_filter = ['name']


# class UpdateDatabaseConfigAdmin(object):
#     list_display = ['public_time', 'modified_time']
#     search_fields = ['public_time']
#     list_filter = ['public_time']


class AboutMeAdmin(object):
    list_display = ['title', 'public_time', 'modified_time']
    search_fields = ['title']
    list_filter = ['title']


xadmin.site.register(Articles, ArticlesAdmin)
xadmin.site.register(Category, CategoryAdmin)
xadmin.site.register(Tag, TagAdmin)
# xadmin.site.register(UpdateDatabaseConfig, UpdateDatabaseConfigAdmin)
xadmin.site.register(About, AboutMeAdmin)
