from django.db import models


# Create your models here.

class SiteVisitor(models.Model):
    ip = models.CharField(max_length=128, verbose_name='来访者ip', primary_key=True)
    ip_type = models.CharField(choices=(('fake', '代理'), ('real', '真实')), default='fake', max_length=6,
                               verbose_name='来访ip类型')
    count = models.BigIntegerField(default=0, verbose_name='来访次数')

    class Meta:
        verbose_name = '访问用户信息'
        verbose_name_plural = verbose_name

    def __str__(self):
        return str(self.ip)


class ArticleVisitor(models.Model):
    article_id = models.IntegerField(verbose_name='对应博客文章')
    ip = models.CharField(max_length=128, verbose_name='来访者ip')

    class Meta:
        verbose_name = '文章访问用户'
        verbose_name_plural = verbose_name

    def __str__(self):
        return str(self.ip)


class DuplicateVisit(models.Model):
    type = models.CharField(max_length=10, default='site', verbose_name='重复访问记录类型')
    count = models.BigIntegerField(default=0, verbose_name='总访问次数')

    class Meta:
        verbose_name = '总访问次数'
        verbose_name_plural = verbose_name

    def __str__(self):
        return str(self.count)
