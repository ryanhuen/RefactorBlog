from BlogStatistics.models import SiteVisitor, ArticleVisitor, DuplicateVisit


def filter_requester_id(request):
    if 'HTTP_X_FORWARDED_FOR' in request.META:  # 获取ip
        client_ip = request.META['HTTP_X_FORWARDED_FOR']
        client_ip = client_ip.split(",")[0]  # 所以这里是真实的ip
        ip_type = 'real'
    else:
        client_ip = request.META['REMOTE_ADDR']  # 这里获得代理ip
        ip_type = 'fake'
    return {
        'client_ip': client_ip,
        'ip_type': ip_type,
    }


def total_visit_record():
    try:
        site_visit = DuplicateVisit.objects.get(type='site')
        site_visit.count += 1
    except Exception:
        site_visit = DuplicateVisit()
        site_visit.type = 'site'
        site_visit.count = 1
    site_visit.save()


def intercept_site_visitor(func):
    def site_visitor_record(request, *args, **kwargs):
        total_visit_record()
        requester_id = filter_requester_id(request)
        if requester_id:
            try:
                visitor = SiteVisitor.objects.get(ip=requester_id['client_ip'])
                visitor.count += 1
                visitor.save()
            except Exception:
                visitor = SiteVisitor()
                visitor.ip = requester_id['client_ip']
                visitor.ip_type = requester_id['ip_type']
                visitor.count = 1
                visitor.save()
        return func(request, *args, **kwargs)

    return site_visitor_record


def intercept_article_visitor(func):
    def article_visitor_record(auth, count, article, request, *args, **kwargs):
        if article:
            requester_id = filter_requester_id(request)
            article_visitor = ArticleVisitor()
            article_visitor.ip = requester_id['client_ip']
            article_visitor.article_id = article.id
            article_visitor.save()
            article.visitor.add(article_visitor)
            article.save()

        return func(auth, count, article, request, *args, **kwargs)

    return article_visitor_record
