import xadmin
from BlogStatistics.models import SiteVisitor, ArticleVisitor, DuplicateVisit
from articles.models import Articles, Category, Tag, About

__author__ = 'ryanhuen'
__data__ = '18-3-19 下午3:49'


class SiteVisitorAdmin(object):
    list_display = ['ip', 'ip_type', 'count']
    search_fields = ['ip']
    list_filter = ['ip', 'ip_type', "count"]


class ArticleVisitorAdmin(object):
    list_display = ['ip', 'article_id']
    search_fields = ['ip']
    list_filter = ['ip', "article_id"]


class DuplicateVisitAdmin(object):
    list_display = ['count']
    search_fields = ['count']
    list_filter = ['count']


xadmin.site.register(SiteVisitor, SiteVisitorAdmin)
xadmin.site.register(ArticleVisitor, ArticleVisitorAdmin)
xadmin.site.register(DuplicateVisit, DuplicateVisitAdmin)
