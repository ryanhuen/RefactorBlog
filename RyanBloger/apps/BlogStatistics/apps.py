from django.apps import AppConfig


class BlogstatisticsConfig(AppConfig):
    name = 'BlogStatistics'
