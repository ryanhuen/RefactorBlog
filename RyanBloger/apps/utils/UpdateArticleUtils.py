import hashlib
from _md5 import md5

from utils import CommonUtils

__author__ = 'ryanhuen'
__data__ = '18/4/23 下午11:44'
import os
import re


class FileList(object):
    file_list = []

    def print_files(self, path):
        lsdir = os.listdir(path)
        dirs = [i for i in lsdir if os.path.isdir(os.path.join(path, i))]
        files = [i for i in lsdir if os.path.isfile(os.path.join(path, i))]
        if files:
            for f in files:
                # print(os.path.join(path, f))
                cur_file_path = os.path.join(path, f)
                if cur_file_path.endswith('.md'):
                    self.file_list.append(cur_file_path[cur_file_path.find('RyanNote'):])
        if dirs:
            for d in dirs:
                self.print_files(os.path.join(path, d))


class OrgConfig(object):
    export = False
    inner_title = ''
    title = ''
    file_path = ''
    category = ''
    tags = []

    def is_not_any_empty(self):
        if self.title and self.file_path and self.category and self.tags:
            return True
        else:
            return False

    def __str__(self) -> str:
        return 'export : %r' % self.export + '\ntitle : ' + self.title + '\nfile_path : ' + self.file_path


def read_org_file_config_content(file_path):
    try:
        f = open(file_path, 'r', encoding='utf-8')  # 标记文件为只读
        result = list()
        org_config = None
        for line in f.readlines():  # 依次读取每行
            line = line.strip()  # 去掉每行头尾空白
            if len(line) and line.startswith('[//]: '):
                result.append(line)
        if result:
            org_config = analysis_config(result, file_path)
        return org_config
    except FileNotFoundError as e:
        return None


def analysis_config(config_list, file_path):
    try:
        org_config = OrgConfig()

        org_config.file_path = file_path[file_path.find('RyanNote'):]
        rex = r'\[//\]\:\s+(?P<key>.*)\s+(:)\s+(?P<value>[\w\w].*)'
        pattern = re.compile(rex)
        for config in config_list:
            if config.find('START_CONFIG') != -1 or config.find('END_CONFIG') != -1:
                continue
            key = pattern.match(config).group('key')
            value = pattern.match(config).group('value')
            if key and key == 'title':
                org_config.title = value
            elif key and key == 'export':
                if value == 'True':
                    org_config.export = True
                else:
                    org_config.export = False
            elif key and key == 'category':
                org_config.category = value.strip().replace(' ', '_')
            elif key and key == 'tags':
                tags = value.strip().split(',')
                org_config.tags = tags
            elif key and key == 'inner_title':
                org_config.inner_title = value
        return org_config
    except Exception as e:
        return None


def check_file_content_md5(content):
    md = hashlib.md5()
    md.update(content.encode('utf-8'))
    return md.hexdigest()


if __name__ == '__main__':
    pass