# 404页面
from django.shortcuts import render


def page_not_found(request):
    return render(request, '404.html')


# 500页面
def server_error(request):
    return render(request, '500.html')
