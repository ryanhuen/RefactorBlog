import requests

__author__ = 'ryanhuen'
__data__ = '18-3-19 下午5:24'


def org2html(org_content):
    # return fetch_node_convert_org(org_content, 'content')
    return org_content

def org2html_toc(org_content):
    # return fetch_node_convert_org(org_content, 'toc')
    return org_content


def fetch_node_convert_org(org_content, fetch_type):
    # 请求api地址
    url = "http://127.0.0.1:3000/api/convert/" + fetch_type
    # 请求参数
    data = {'content': org_content}
    # 执行请求
    response = requests.post(url, json=data)
    # # 查看执行的url
    # print('\n查看请求执行的url:\n', response.url)
    # # 获得请求的内容
    # print('\n获得请求的内容:\n', response.text)
    return response.text


def read_file_content(file_path):
    try:
        f = open(file_path, 'r', encoding='utf-8')  # 标记文件为只读
        text = f.read()
        return text
    except FileNotFoundError as e:
        return '数据无法读取'


def crop_image(image):
    if image.width > image.height:
        x = (image.width - image.height) / 2
        y = 0
        box = (x, y, x + image.height, y + image.height)
        region = image.crop(box)
        return region
    else:
        x = 0
        y = (image.height - image.width) / 2
        box = (x, y, x + image.width, y + image.width)
        region = image.crop(box)
        return region